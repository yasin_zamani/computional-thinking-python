import random    # <---- FIX!

def random_echo(text):
    ''' (str) -> str

    Return text repeated a random number of times, a minimum of 0 and a
    maximum of 10 times,separated by a comma and a space. The last occurence
    has no comma (norspace).
    '''
    val= random.randint(0,10)
    if(val==0):
        return ""
    else:
        string=(text+", ")*(val-1)
        string =string+text
        return string


def test_random_echo():
    text = random_echo("Hi")
    print(text)
    print(random_echo("Yo"))
    print(random_echo("Wazzzup!"))

        

# Do not touch below:
if __name__ == "__main__":
    import sys

    if len(sys.argv) == 1:
        test_random_echo()
    else:
        from random import seed
        for test_case in sys.stdin.readlines():
            params = test_case.split()
            as_int = int(params[0])
            seed(as_int)
            print(random_echo(params[1]))


