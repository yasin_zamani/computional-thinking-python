def credit_card_type(income, history):
    ''' (number, str) -> str

    Returns the type of credit card based in the income and history of the
    client.
    '''
    if (income < 20000):
        card_type = "N/A"
    elif(income >= 20000 and income < 30000):
        if(history == 'Fair' or history == 'fair'):
            card_type = 'Standard'
        elif(history == 'Good' or history == 'good'):
            card_type = 'Bronze'
        elif(history == 'Excellent' or history == 'excellent'):
            card_type = 'Gold'
    elif(income >= 30000 and income < 40000):
        if(history == 'Fair' or history =='fair'):
            card_type = 'Bronze'
        elif(history == 'Good' or history == 'good'):
            card_type = 'Gold'
        elif(history == 'Excellent' or history == 'excellent'):
            card_type = 'Platinum'
    else:
        if(history == 'Fair' or history == 'fair'):
            card_type = 'Gold'
        elif(history == 'Good' or history == 'good'):
            card_type = 'Platinum'
        elif(history == 'Excellent' or history == 'excellent'):
            card_type = 'Diamond'

    return card_type


# Do not touch below:
def credit_card_query():
    income = float(input("Client's income: "))
    credit_history = input("Client's credit history (Fair/Good/Excellent): ")

    card_type = credit_card_type(income, credit_history)
    
    if card_type == "N/A":
        print("That client can't have a credit card, sorry.")
    else:
        print("A client with income of Â£" + str(income), "and \"" +\
              credit_history + "\" credit history, gets a", card_type,\
              "credit card.")
credit_card_query()
# Do not touch below:
if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        for test_case in sys.stdin.readlines():
            test = test_case.split()
            if len(test) != 2:
                break
            print(credit_card_type(int(test[0]), test[1]))
