SECONDS_PER_MINUTE = 60
MINUTES_PER_HOUR = 60

# Returns the number of seconds, given a number of minutes
def minutes_to_seconds(minutes):
    return minutes * SECONDS_PER_MINUTE


# Returns the number of minutes, given a number of hours
def hours_to_minutes(hours):
    return hours * MINUTES_PER_HOUR

# Use the two previous functions (don't use literals nor the previous constants)
def hours_to_seconds(hours):
    return hours * MINUTES_PER_HOUR * MINUTES_PER_HOUR          # <--- FIX


# Here you just need to use two of the three previously defined functions
def time_to_seconds(hours, minutes, seconds):
    seconds = seconds
    total = minutes_to_seconds(minutes) + hours_to_seconds(hours) + seconds # might have broken the rules here
    return total
    


# Test Zone. You can play here (i.e. you can modify the body of this function):
def test_time_to_seconds():
    secs = time_to_seconds(10, 30, 1)
    print(secs)
    print(time_to_seconds(0, 1, 0))


# Do not touch below:
if __name__ == "__main__":
    import sys

    if len(sys.argv) == 1:
        test_time_to_seconds()
    else:
        for test_case in sys.stdin.readlines():
            as_ints = [int(n) for n in test_case.split()]
            if len(as_ints) != 3:
                break
            print(time_to_seconds(*as_ints))
