# Paste your function below this comment (only the requested function, no tests)
def celsius_to_fahrenheit(celsius):
	return (celsius * 9 / 5 + 32

# Do not modify below this line
def my_tests():
    celsius = 20
    print(celsius, "C is", celsius_to_fahrenheit(celsius), "F")

    celsius = 0
    print(celsius, "C is", celsius_to_fahrenheit(celsius), "F")

    celsius = -2
    print(celsius, "C is", celsius_to_fahrenheit(celsius), "F")

    celsius = 50
    print(celsius, "C is", celsius_to_fahrenheit(celsius), "F")

    celsius = -6.55
    print(celsius, "C is", celsius_to_fahrenheit(celsius), "F")
my_tests()
