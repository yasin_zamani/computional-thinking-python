def volume_of_cube(length):
    '''
    (int) - > int
    returns volume of cube.
    '''
    power_of_3 =  pow(length, 3)
    return power_of_3

def test_volume_of_cube():
    len_size = int(input("length side : "))
    print("The volume is : " , volume_of_cube(len_size))
