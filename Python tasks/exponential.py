from math import *

def maths(number):
    '''
    (number) - > float
    returns our own formula of exp.
    >>> test_exp()
    Type a number in:1
    My exp 2.72
    Math's exp 2.72
    The difference 0.0
    >>> test_exp()
    Type a number in:2
    My exp 7.27
    Math's exp 7.39
    The difference 0.12
    >>> test_exp()
    Type a number in:-1
    My exp 0.37
    Math's exp 0.37
    The difference 0.0
    >>> 
    '''
    exp1 = 1+(number/factorial(1))+pow(number,2)/factorial(2)+pow(number,3)/factorial(3)+pow(number,4)/factorial(4)+pow(number,5)/factorial(5)
    return exp1


def test_exp():
    exp2 = float(input("Type a number in:"))
    my_exp = print("My exp",round(maths(exp2),2))
    math_exp=print("Math's exp", round(exp(exp2),2))
    difference=print("The difference", abs(round(maths(exp2)- exp(exp2),2)))
    
