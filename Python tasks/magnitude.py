def square_root(number):
    '''(number) -> float

    Returns the square root of the input number.

    >>> square_root(0)
    0
    >>> square_root(4)
    2
    >>> square_root(9.0)
    3.0
    '''
    return  number**0.5

def magnitude(mag1, mag2):
    """ (number, number) -> float
    Returns formula  square_root and inputs mag1 and mag2 rounds to tenth decimal

    >>> magnitude(1,1)
    1.41

    """
    return round(square_root(mag1**2 + mag2**2), 2)
