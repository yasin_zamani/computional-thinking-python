from random import *
"""
() - > str
>>> get_rand_weekday()
'Wednesday'
>>> get_rand_weekday()
'Friday'
>>> get_rand_weekday()
'Tuesday'
>>> get_rand_weekday()
'Sunday'
>>> get_rand_weekday()
'Sunday'
>>> get_rand_weekday()
'Monday'
>>> get_rand_weekday()
'Monday'
>>> get_rand_weekday()
'Thursday'
>>> 
"""
def get_rand_weekday():
    val = randint(1,7)
    if val == 1:
        return "Monday"
    elif val == 2:
        return "Tuesday"
    elif val == 3:
        return "Wednesday"
    elif val == 4:
        return "Thursday"
    elif val == 5:
        return "Friday"
    elif val == 6:
        return "Saturday"
    else:
        return "Sunday"
