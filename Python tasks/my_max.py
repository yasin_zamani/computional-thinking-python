def my_max(number, number2):
    '''
    (number, number) -> number
    if statement number greater than number 2
    '''
    if number > number2:
        return number
    else:
        return number2

def test_my_max():
    number = float(input("Insert a number : "))
    number2 = float(input("Insert another number : "))
    print("Greatest between the two is: " , my_max(number, number2))
test_my_max()
        
