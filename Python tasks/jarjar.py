def jar_jar():
    '''() -> str

    Returns the DNA sequence of Jar Jar Binks.
    '''
    start_end= 'TACTAC'
    dna_1='GATACAA'
    dna_2='CATCAT'
    dna_3='TA'
    sum_4=start_end+((dna_1*20+dna_2*10+dna_3*8)*40)+start_end
    return sum_4


def test_jar_jar():
    '''() -> str
    DNA is calculated by str with an integer.
    '''
    dna = jar_jar()
    print(dna)
    print('Length of DNA:', len(dna))


# Just calling the test
test_jar_jar()  # Don't touch.
