def length_of_two(string1, string2):
    '''
    (str, str) -> None
    saves the total length of two strings
    and returns it.
    '''
    total = len(string1) + len(string2)
    return total

def test_length_of_two():
    first_name = input("Type in your firstname : ")
    last_name = input("Type in your last name: ")
    total = length_of_two(first_name, last_name)
    print("The number of characters on your name is" , total)
    
    
    
