def my_abs(number):
    '''
    (number) - > number
    Number greater than 0 returns the number
    else makes it positive.
    '''
    if number > 0:
        return number
    else:
        return number*(-1)
        


def test_my_abs():
    number_1 = float(input("Insert a number : "))
    result = my_abs(number_1)
    print("The absolute value is", result)
test_my_abs()    
    
